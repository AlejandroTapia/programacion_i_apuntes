from email_validator import validate_email, EmailNotValidError
from typing import Optional


class User:
    def __init__(self, name: str,
                 email: str = None):
        """
        Constructor
        :param name: Name´s user
        :param email: user´s email, it´s optional
        """
        self.name = name
        if email is None:
            self._email = None
        else:
            self._email = self.validate(email)

    @staticmethod
    def validate(email: str) -> Optional[str]:
        try:
            # Validate.
            valid = validate_email(email)

            # Update with the normalized form.
            v_email = valid.email
        except EmailNotValidError as e:
            # email is not valid, exception message is human-readable
            print(str(e))
            return None

        return v_email

    def __str__(self):
        if self._email is None:
            return f'User: {self.name}'
        else:
            return f'User: {self.name}, email: {self._email}'


user_1 = User('Rob', 'rvazquez@ufv.es')
user_2 = User('Bob', 'this@isabedemail')
