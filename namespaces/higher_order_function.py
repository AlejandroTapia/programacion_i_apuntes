from typing import Callable


# Function that returns a function that exponentiates to a given number.
def f(n: int) -> Callable:
    def g(x: int) -> int:
        return x ** n

    return g


h = f(3)
print(h(2))
