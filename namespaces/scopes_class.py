from typing import Final

A_CONSTANT: Final = 3


def printing():
    y = 3
    def calculation(x) -> int:
        nonlocal y
        print(y)
        y = x ** A_CONSTANT
        return y
    z = calculation(y)
    print(y)

printing()
