import numpy as np

def fib_dynamic_array(n: int) -> int:
    """
    Fibonacci Series using Dynamic Programming, using array
    :param n: position in the sequence
    :return: Fn
    """
    if n < 2:
        return n
    table = np.zeros(shape=(n + 1,), dtype=int)
    table[1] = 1
    for i in range(2, n + 1):
        table[i] = table[i - 1] + table[i - 2]
    return table[n]


print(fib_dynamic_array(3))