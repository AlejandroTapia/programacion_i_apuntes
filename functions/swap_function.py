#Funcion swap

def swap (a: int, b: int) -> tuple[int, int]:
    return b, a


a = 2
b = 3

x, y = swap(a, b)
print (x, ',', y)
