# Square function
A_CONSTANT = 3
#las constantes en MAYUSCULAS


def square(n: int) -> int:
    """
    A function that squares.
    :param n: an integer
    :return: a squared integer
    """
    return n ** 2


a= square(A_CONSTANT)
print(a)

#otra forma
print(square(A_CONSTANT))

# Guardar el resultado en una variable
result = square(4)
print(result)

# Manda el resultado del square a otra funcion
print(square(5))
# resultado de la funcion con un if expression

if square(3) < 15:
    print('Still less than 15')
