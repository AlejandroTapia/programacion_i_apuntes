# Fibonacci function

def fibonacci(n: int) -> int:
    """
    A function that gives the sum of the last two terms of the sequence
    :param n: an integer
    :return: the sum of the last two terms of the sequence
    """
    if n >= 2:
        return fibonacci(n - 1) + fibonacci(n - 2)
    elif n < 0:
        print('El valor tiene que ser mayor a 0')
    else:
        return n


# Examples of same solutions to prove function works properly
print(fibonacci(8))
print(fibonacci(10))
print(fibonacci(-3))
print(fibonacci(0))

#Print a value by screen
x = int(input("Ingrese un numero entero: "))
print(f"El {x} valor de la sucesion de fibonacci es : {fibonacci(x)} ")
