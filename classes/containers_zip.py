student_ids = ['001', '002', '004']
names = ['John', 'Peter', 'Mary']
surnames = ['Johnson', 'Doe', 'Peterson']

students = {}
for student_id, name, surname in zip(student_ids, names, surnames):
    students[student_id] = Student(name, surname)

students['004'].first_name = 'Mary Joe'
#zip sirve