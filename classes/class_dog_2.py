class Dog:
    def __init__(self,
                 name: str,
                 color: str,
                 tricks: set = None):
        """
        Create a dog object
        :param name: name of the dog
        :param tricks: abilities of the dog
        """
        self.name = name
        self.color = color
        if tricks is None:
            self.tricks = {[]}  # o set() para crear un diccionario vacio
        else:
            self.tricks = tricks
