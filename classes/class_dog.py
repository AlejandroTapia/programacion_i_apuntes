class Dog:
    def __init__(self, name: str):
        """
        Dog object
        :param name: Name of the dog
        """
        self.name = name
        self.tricks = []  # lista vacía de habilidades

    def add_tricks(self, trick: str) -> None:
        """
        Adds an ability to a dog
        :param trick: new ability added to the dog
        """
        self.tricks.append(trick)


a = Dog('Yacky')
a.add_tricks('Dar la patita')

b = Dog('Willy')
b.add_tricks('Sentarse')
b.add_tricks('tumbarse')

print(a.name)
print(b.tricks)

