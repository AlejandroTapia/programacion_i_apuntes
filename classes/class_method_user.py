class User:
    # class variable, list of users id
    user_list = []

    def __init__(self, name: str, user_id: str):
        """
        Constructor
        :param name: name of the user
        :param user_id: id of the user
        """
        self.name = name
        self.user_id = user_id

    @classmethod
    def new_user(cls, name: str, user_id: str):
        cls.user_list.append(user_id)
        return cls(name, user_id)


# Examples of some instances
p = User.new_user('Bob', '123')
e = User.new_user('Peter', '234')

