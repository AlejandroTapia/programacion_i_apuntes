class Complex:
    def __init__(self, realpart: float = 0.0, imagpart: float = 0.0):
        """
        Complex number constructor
        :param realpart: real part of the complex number
        :param imagpart: imaginary part of the complex number
        """
        self.r = realpart
        self.i = imagpart

    def abs(self):
        """
        Computes absolute value of the complex number
        :return: the absolute value
        """
        return (self.r ** 2 + self.i ** 2) ** 0.5


# ** 0.5 es lo mismo que hacer la raiz cuadrada, elevas a 1/2. para usar sqrt hay que importarlo
# esta funcion sirve para calcular el valor absoluto de los numeros imaginarios por definicion
# en el debugger creamos una nueva instancia, q = Complex (1, 2)
# podemos calcular el valor absoluto de la nueva instancia q.abs()

x = Complex(3.0, -4.5)
print(x)
print(x.r, x.i)
