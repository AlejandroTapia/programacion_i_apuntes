class User:
    user_dic = {}

    def __init__(self, name: str, user_id: str):
        self.name = name
        self.user_id = user_id

    @classmethod
    def new_user(cls, name: str, user_id: str):
        cls.user_dic.update({user_id: cls(user_id,name)})
        return cls.user_dic()


p = User.new_user('Bob', '123')
e = User.new_user('Peter', '234')