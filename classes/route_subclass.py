import datetime as dt


class WayPoint:
    def __init__(self, lat: float, lon: float, time: dt.datetime):
        self.lat = lat
        self.lon = lon
        self.time = time


class RouteSteps:
    def __init__(self, driver: str, wp: WayPoint):
        self.driver = driver
        self.wp = wp

    @classmethod
    def from_values(cls, driver: str, lat: float, lon: float,
                    year: int, month: int, day: int):
        return cls(driver, WayPoint(lat, lon,
                                    dt.datetime(year, month, day)))


class Delivery(RouteSteps):
    def __init__(self, driver: str, wp: WayPoint, receiver: str):
        super().__init__(driver, wp)
        self.receiver = receiver


class Pickup(RouteSteps):
    def __init__(self, driver: str, wp: WayPoint, sender: str):
        super().__init__(driver, wp)
        self.sender = sender


class Report:
    def __init__(self, report: str, level: int, wp: WayPoint):
        self.report = report
        self.level = level
        self.wp = wp
