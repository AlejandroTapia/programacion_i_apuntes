import datetime as dt


class WayPoint:
    def __init__(self, lat: float, lon: float, time: dt.datetime):
        self.lat = lat
        self.lon = lon
        self.time = time


class Delivery:
    def __init__(self, driver: str, wp: WayPoint):
        self.driver = driver
        self.wp = wp

    @classmethod
    def from_values(cls, driver: str, lat: float, lon: float,
                    year: int, month: int, day: int):

        return cls(driver, WayPoint(lat, lon,
                                    dt.datetime(year, month, day)))


class Report:
    def __init__(self, report: str, level: int, wp: WayPoint):
        self.report = report
        self.level = level
        self.wp = wp


# d= dt.datetime(2021, 1, 1)
# wpoint = WayPoint(3.0, 5.0, d)
# wpoint = WayPoint(3.0, 5.0, dt.datetime(2021, 1, 1))
# d = Delivery('Pepito', wp=wpoint)
d = Delivery('Pepito', WayPoint(40.3, 2.71, dt.datetime(2020, 1, 1)))
d.wp.lat


e = Delivery.from_values('Pepito')
