AVAILABILITY = {
    'Mon': [9, 17],
    'Tue': [9, 17],
    'Wed': [9, 17],
    'Thu': [9, 17],
    'Fri': [9, 12]
}


class Vehicle:
    def __init__(self, license_plate: str):
        self.license_plate = license_plate
        self.availability = AVAILABILITY

    def set_availability(self, week_day: str, availability: list[int, int]):
        """
        Set availability for a given day
        :param week_day: day of the week
        :param availability: list len 2 with start and end time
        """
        if week_day in AVAILABILITY.keys():
            self.availability[week_day] = availability
            # self.availability.update({week_day: availability})

    def get_availability(self, week_day: str) -> list[int, int]:
        """
        Return availability for a given day
        :param week_day: day of the week
        :return: a list len 2 with start and end time
        """
        return self.availability.get(week_day)


vehicle_1 = Vehicle("4444LHR")
vehicle_1.set_availability('Mon', [9, 10])
vehicle_1.get_availability('Mon')
