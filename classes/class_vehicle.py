days_available = {
    'Mon': [9, 17],
    'Tue': [9, 17],
    'Wed': [9, 17],
    'Thu': [9, 17],
    'Fri': [9, 12]
}
# We defined the default availability


class Vehicle:
    def __init__(self, license_plate: str):
        """
        Object vehicle
        :param license_plate: license plate of the vehicle
        """
        self.license_plate = license_plate
        self.days_available = days_available

    def update_availability(self, day, hours):
        """
        Object that update the availability of any day
        :param day:day availability of vehicles
        :param hours:list of new hours available of one day
        """
        self.days_available.update({day: hours})
        print(f"El horario actualizado del dia {day}, es : {days_available[day]}")


ferrari = Vehicle('4444LHR')
ferrari.update_availability('Thu', [10, 14])
toyota = Vehicle('7512JDR')
toyota.update_availability('Mon', [9, 17])
